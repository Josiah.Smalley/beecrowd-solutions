SELECT life.name, ROUND(omega * 1.618, 3) AS "The N Factor"
FROM life_registry             life
         INNER JOIN dimensions d ON life.dimensions_id = d.id
WHERE life.name LIKE 'Richard %'
  AND (d.name = 'C875'
    OR d.name = 'C774')
ORDER BY omega;