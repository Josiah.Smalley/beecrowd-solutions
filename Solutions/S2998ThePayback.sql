WITH RollingReturns AS (
                       SELECT client_id,
                              month,
                              (
                              SELECT SUM(profit)
                              FROM operations o2
                              WHERE operations.month >= o2.month
                                AND operations.client_id = o2.client_id
                              ) AS rollingSum
                       FROM operations
                       ),
     Results AS (
     SELECT id,
            name,
            investment,
            (
            SELECT MIN(month)
            FROM RollingReturns
            WHERE RollingReturns.client_id = clients.id
              AND rollingSum >= clients.investment
            ) AS month_of_payback
     FROM clients
     )
SELECT Results.name,
       Results.investment,
       Results.month_of_payback,
       (
       SELECT rollingSum - Results.investment
       FROM RollingReturns
       WHERE RollingReturns.client_id = Results.id
         AND month_of_payback = RollingReturns.month
       ) AS return
FROM Results
WHERE month_of_payback IS NOT NULL
ORDER BY return DESC;