-- The output for this problem was far too picky. It required exactly two decimals for all non-zero values for salaries
-- which caused me to use rounding, to_char, and trim; rounding should have been sufficient.
WITH EmployeeSalaries AS (
                         SELECT emp.matr,
                                COALESCE(( -- this sub query finds all positive salary values
                                         SELECT SUM(v.valor)
                                         FROM emp_venc                 ev
                                                  LEFT JOIN vencimento v ON ev.cod_venc = v.cod_venc
                                         WHERE ev.matr = emp.matr
                                         ), 0)
                                    - COALESCE(( -- this sub query finds all negative salary values
                                               SELECT SUM(d.valor)
                                               FROM emp_desc               ed
                                                        LEFT JOIN desconto d ON d.cod_desc = ed.cod_desc
                                               WHERE ed.matr = emp.matr
                                               ), 0) AS salary
                         FROM empregado AS emp
                         ORDER BY emp.matr
                         OFFSET 0 ROWS
                         )
SELECT dep.nome                                                           AS "Nome Departamento",
       COUNT(DISTINCT emp.matr)                                           AS "Numero de Empregados",
       TRIM(TO_CHAR(ROUND(AVG(salary), 2), '9999999999.99'))              AS "Media Salarial",
       TRIM(TO_CHAR(ROUND(MAX(salary), 2), '9999999999.99'))              AS "Maior Salario",
       TRIM(CASE
                WHEN MIN(salary) = 0 THEN '0'
                ELSE TO_CHAR(ROUND(MIN(salary), 2), '9999999999.99') END) AS "Menor Salario"
FROM departamento                    dep
         INNER JOIN divisao          div ON dep.cod_dep = div.cod_dep
         INNER JOIN empregado        emp ON emp.lotacao_div = div.cod_divisao
         INNER JOIN EmployeeSalaries ES ON ES.matr = EMP.matr
GROUP BY dep.nome
ORDER BY "Media Salarial" DESC;