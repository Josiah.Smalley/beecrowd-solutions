SELECT products.name as product_name, providers.name as provider_name
FROM products
         INNER JOIN providers ON products.id_providers = providers.id
WHERE id_categories = 6;