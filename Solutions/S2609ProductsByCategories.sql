SELECT C.name, SUM(p.amount)
FROM categories              C
         INNER JOIN products P ON C.id = P.id_categories
GROUP BY C.name;
