using System;

namespace Solutions {
    public class S1010SimpleCalculate {

        public static void Main(string[] args) {
            InputReader.Skip();
            var units1 = InputReader.NextInt();
            var price1 = InputReader.NextDouble();
            InputReader.Skip();
            var units2 = InputReader.NextInt();
            var price2 = InputReader.NextDouble();

            Console.WriteLine(CalculateResult(units1, price1, units2, price2));
        }

        public static string CalculateResult(int units1, double price1, int units2, double price2) {
            var total = (units1 * price1) + (units2 * price2);
            return $"VALOR A PAGAR: R$ {total:F2}";
        }


        private class InputReader {

            // This class would, ideally, be re-used between multiple solutions. However, due to the nature of the 
            // beecrowd platform, we cannot have more than one file uploaded per submission.

            private static string[] _sections = Array.Empty<string>();
            private static int _index;

            public static bool HasNextSection => _index < _sections.Length;

            public static void LoadSections() {
                _sections = ReadLine().Split(' ');
                _index = 0;
            }

            public static void Skip(int count = 1) {
                while (count > 0) {
                    if (_index + count <= _sections.Length) {
                        _index += count;
                        count = 0;
                        continue;
                    }

                    count -= _sections.Length - _index;
                    LoadSections();
                }
            }

            public static string NextSection() {
                if (!HasNextSection) {
                    LoadSections();
                }

                if (!HasNextSection) {
                    throw new NullReferenceException("Unable to load the next section");
                }

                var result = _sections[_index];
                _index++;
                return result;
            }

            public static double ReadDouble() {
                var line = Console.ReadLine();

                if (line == null || !double.TryParse(line, out var result)) {
                    throw new NullReferenceException($"Line cannot be parsed as double: {line}");
                }

                return result;
            }

            public static int ReadInt() {
                var line = Console.ReadLine();

                if (line == null || !int.TryParse(line, out var result)) {
                    throw new NullReferenceException($"Line cannot be parsed as int: {line}");
                }

                return result;
            }

            public static string ReadLine() {
                var line = Console.ReadLine();

                if (line == null) {
                    throw new NullReferenceException($"Line is null");
                }

                return line;
            }

            public static int NextInt() {
                var section = NextSection();

                if (!int.TryParse(section, out var result)) {
                    throw new NullReferenceException($"Line cannot be parsed as int: {section}");
                }

                return result;
            }

            public static double NextDouble() {

                var section = NextSection();

                if (!double.TryParse(section, out var result)) {
                    throw new NullReferenceException($"Line cannot be parsed as double: {section}");
                }

                return result;
            }

        }

    }
}
