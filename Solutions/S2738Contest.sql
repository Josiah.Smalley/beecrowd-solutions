SELECT name, ROUND((2 * math + 3 * specific + 5 * project_plan) / 10, 2) AS avg
FROM candidate
         LEFT JOIN score ON candidate.id = score.candidate_id
ORDER BY avg DESC;