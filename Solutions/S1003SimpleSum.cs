using System;

namespace Solutions {
    public class S1003SimpleSum {

        public static void Main(string[] args) {
            var a = ReadInt();
            var b = ReadInt();

            Console.WriteLine(CalculateResult(a, b));
        }

        public static string CalculateResult(int a, int b) => $"SOMA = {a + b}";


        private static int ReadInt() {
            var line = Console.ReadLine();

            if (line == null || !int.TryParse(line, out var result)) {
                throw new NullReferenceException($"Input line cannot be parsed as int: {line}");
            }

            return result;
        }

    }
}
