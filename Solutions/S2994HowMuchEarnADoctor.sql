SELECT doctors.name,
       ROUND(SUM(hours * (1 + (bonus * 0.01)) * 150), 1) AS salary
FROM doctors
         INNER JOIN attendances ON attendances.id_doctor = doctors.id
         INNER JOIN work_shifts ON work_shifts.id = attendances.id_work_shift
GROUP BY doctors.id
ORDER BY salary DESC;