using System;

namespace Solutions {
    public class S1009SalaryWithBonus {

        public static void Main(string[] args) {
            var name = InputReader.ReadLine();
            var salary = InputReader.ReadDouble();
            var sales = InputReader.ReadDouble();
            
            Console.WriteLine(CalculateResult(name, salary, sales));
        }
        
        public static string CalculateResult(string name, double salary, double sales) {
            var total = salary + (sales * 0.15);
            return $"TOTAL = R$ {total:F2}";
        }

        private class InputReader {

            // This class would, ideally, be re-used between multiple solutions. However, due to the nature of the 
            // beecrowd platform, we cannot have more than one file uploaded per submission.
            public static double ReadDouble() {
                var line = Console.ReadLine();

                if (line == null || !double.TryParse(line, out var result)) {
                    throw new NullReferenceException($"Line cannot be parsed as double: {line}");
                }

                return result;
            }

            public static int ReadInt() {
                var line = Console.ReadLine();

                if (line == null || !int.TryParse(line, out var result)) {
                    throw new NullReferenceException($"Line cannot be parsed as int: {line}");
                }

                return result;
            }

            public static string ReadLine() {
                var line = Console.ReadLine();
                
                if (line == null) {
                    throw new NullReferenceException($"Line is null");
                }

                return line;
            }

        }


    }
}
