SELECT P.id, P.name
FROM products                  P
         INNER JOIN categories c ON P.id_categories = c.id
WHERE c.name LIKE 'super%';