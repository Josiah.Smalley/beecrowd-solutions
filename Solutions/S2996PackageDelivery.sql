SELECT year, u1.name AS sender, u2.name AS receiver
FROM packages
         INNER JOIN users u1 ON u1.id = packages.id_user_sender
         INNER JOIN users u2 ON u2.id = packages.id_user_receiver
WHERE (color = 'blue' OR year = 2015)
  AND 'Taiwan' NOT IN (u1.address, u2.address)
ORDER BY year DESC;