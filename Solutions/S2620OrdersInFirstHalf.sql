SELECT C.name, O.id
FROM customers             C
         INNER JOIN orders O ON C.id = O.id_customers
WHERE O.orders_date >= '2016-01-01'
  AND O.orders_date < '2016-07-01';