WITH EmployeePositive AS (
                         SELECT empregado.matr AS employeeId, SUM(COALESCE(vencimento.valor, 0)) AS value
                         FROM empregado
                                  LEFT JOIN emp_venc ON emp_venc.matr = empregado.matr
                                  LEFT JOIN vencimento ON vencimento.cod_venc = emp_venc.cod_venc
                         GROUP BY empregado.matr
                         ),
     EmployeeNegative AS (
     SELECT empregado.matr AS employeeId, SUM(COALESCE(desconto.valor, 0)) AS value
     FROM empregado
              LEFT JOIN emp_desc ON empregado.matr = emp_desc.matr
              LEFT JOIN desconto ON desconto.cod_desc = emp_desc.cod_desc
     GROUP BY empregado.matr
     ),
     EmployeeNet AS (
     SELECT P.employeeId, P.value - N.value AS netSalary
     FROM EmployeePositive                P
              INNER JOIN EmployeeNegative N ON P.employeeId = N.employeeId
     ),
     DivisionAverages AS (
     SELECT empregado.lotacao_div AS divisionId, AVG(EmployeeNet.netSalary) AS avgSalary
     FROM empregado
              INNER JOIN EmployeeNet ON EmployeeNet.employeeId = empregado.matr
     GROUP BY empregado.lotacao_div
     )
SELECT empregado.nome, ROUND(EmployeeNet.netSalary, 2)
FROM empregado
         INNER JOIN DivisionAverages ON DivisionAverages.divisionId = empregado.lotacao_div
         INNER JOIN EmployeeNet ON EmployeeNet.employeeId = empregado.matr
WHERE netSalary >= 8000
  AND netSalary > DivisionAverages.avgSalary
ORDER BY empregado.lotacao_div;