using System;

namespace Solutions {
    public class S1008Salary {

        public static void Main(string[] args) {
            var employeeNumber = InputReader.ReadInt();
            var hoursWorked = InputReader.ReadInt();
            var hourlyWage = InputReader.ReadDouble();
            
            Console.WriteLine(CalculateResult(employeeNumber, hoursWorked, hourlyWage));
        }
        public static string CalculateResult(int employeeNumber, int hoursWorked, double hourlyWage) {
            var salary = hourlyWage * hoursWorked;
            
            return $"NUMBER = {employeeNumber}\nSALARY = U$ {salary:F2}";
        }

        private class InputReader {

            // This class would, ideally, be re-used between multiple solutions. However, due to the nature of the 
            // beecrowd platform, we cannot have more than one file uploaded per submission.
            public static double ReadDouble() {
                var line = Console.ReadLine();

                if (line == null || !double.TryParse(line, out var result)) {
                    throw new NullReferenceException($"Line cannot be parsed as double: {line}");
                }

                return result;
            }

            public static int ReadInt() {
                var line = Console.ReadLine();

                if (line == null || !int.TryParse(line, out var result)) {
                    throw new NullReferenceException($"Line cannot be parsed as int: {line}");
                }

                return result;
            }

        }


    }
}
