WITH Counts AS (
               SELECT amount, COUNT(*) AS counts
               FROM value_table
               GROUP BY amount
               ),
     MaxCount AS (
     SELECT MAX(counts) AS maximum
     FROM counts
     )
SELECT amount
FROM Counts
         CROSS JOIN MaxCount
WHERE counts = maximum;