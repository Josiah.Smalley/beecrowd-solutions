WITH EmployeePositive AS (
                         SELECT empregado.matr AS employeeId, SUM(COALESCE(vencimento.valor, 0)) AS value
                         FROM empregado
                                  LEFT JOIN emp_venc ON emp_venc.matr = empregado.matr
                                  LEFT JOIN vencimento ON vencimento.cod_venc = emp_venc.cod_venc
                         GROUP BY empregado.matr
                         ),
     EmployeeNegative AS (
     SELECT empregado.matr AS employeeId, SUM(COALESCE(desconto.valor, 0)) AS value
     FROM empregado
              LEFT JOIN emp_desc ON empregado.matr = emp_desc.matr
              LEFT JOIN desconto ON desconto.cod_desc = emp_desc.cod_desc
     GROUP BY empregado.matr
     )
SELECT d.nome, emp.nome, EP.value AS gross, EN.value AS discounts, EP.value - EN.value AS net
FROM empregado                       emp
         INNER JOIN divisao          div ON emp.lotacao_div = div.cod_divisao
         INNER JOIN departamento     d ON d.cod_dep = div.cod_dep
         INNER JOIN EmployeePositive EP ON EP.employeeId = emp.matr
         INNER JOIN EmployeeNegative EN ON EN.employeeId = emp.matr
ORDER BY net DESC;