using System;

namespace Solutions {
    public class S1002AreaOfACircle {
        private const double Pi = 3.14159;
        public static void Main(string[] args) {
            var radius = ReadDouble();
            Console.WriteLine(CalculateResult(radius));
        }

        public static string CalculateResult(double radius) {
            var area = Pi * radius * radius;

            return $"A={area:f4}";
        }

        private static double ReadDouble() {
            var line = Console.ReadLine();

            if (line == null || !double.TryParse(line, out var result)) {
                throw new NullReferenceException($"Line is null or cannot be parsed as a double: {line}");
            }

            return result;
        }
    }
}
