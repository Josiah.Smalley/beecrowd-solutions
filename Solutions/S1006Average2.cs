using System;

namespace Solutions {
    public class S1006Average2 {

        private const double WeightA = 2;
        private const double WeightB = 3;
        private const double WeightC = 5;
        private const double WeightTotal = WeightA + WeightB + WeightC;

        public static void Main(string[] args) {
            var a = InputReader.ReadDouble();
            var b = InputReader.ReadDouble();
            var c = InputReader.ReadDouble();

            Console.WriteLine(CalculateResult(a, b, c));
        }

        public static string CalculateResult(double a, double b, double c) {
            var weightedA = a * WeightA;
            var weightedB = b * WeightB;
            var weightedC = c * WeightC;
            var total = (weightedA + weightedB + weightedC) / WeightTotal;

            return $"MEDIA = {total:f1}";
        }

        private class InputReader {

            // This class would, ideally, be re-used between multiple solutions. However, due to the nature of the 
            // beecrowd platform, we cannot have more than one file uploaded per submission.
            public static double ReadDouble() {
                var line = Console.ReadLine();

                if (line == null || !double.TryParse(line, out var result)) {
                    throw new NullReferenceException($"Line cannot be parsed as double: {line}");
                }

                return result;
            }

        }

    }
}
