using System;

namespace Solutions {
    public class S1001ExtremelyBasic {

        public static void Main(string[] args) {
            var a = ReadInt();
            var b = ReadInt();
            Console.WriteLine($"X = {a+b}");
        }

        private static int ReadInt() {
            var line = Console.ReadLine();

            if (line == null || !int.TryParse(line, out var result)) {
                throw new NullReferenceException($"Input is null or not an integer: {line}");
            }

            return result;
        }

    }
}
