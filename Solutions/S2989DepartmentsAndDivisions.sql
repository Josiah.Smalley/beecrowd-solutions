WITH EmployeeSalaries AS (
                         SELECT emp.matr,
                                COALESCE(( -- this sub query finds all positive salary values
                                         SELECT SUM(v.valor)
                                         FROM emp_venc                 ev
                                                  LEFT JOIN vencimento v ON ev.cod_venc = v.cod_venc
                                         WHERE ev.matr = emp.matr
                                         ), 0)
                                    - COALESCE(( -- this sub query finds all negative salary values
                                               SELECT SUM(d.valor)
                                               FROM emp_desc               ed
                                                        LEFT JOIN desconto d ON d.cod_desc = ed.cod_desc
                                               WHERE ed.matr = emp.matr
                                               ), 0) AS salary
                         FROM empregado AS emp
                         ORDER BY emp.matr
                         OFFSET 0 ROWS
                         )
SELECT dep.nome              AS department_name,
       div.nome              AS division_name,
       ROUND(AVG(salary), 2) AS average,
       ROUND(MAX(salary), 2) AS maximum
FROM departamento                    dep
         INNER JOIN divisao          div ON dep.cod_dep = div.cod_dep
         INNER JOIN empregado        emp ON emp.lotacao_div = div.cod_divisao
         INNER JOIN EmployeeSalaries ES ON ES.matr = EMP.matr
GROUP BY dep.nome, div.nome
ORDER BY average DESC;