SELECT DISTINCT C.id, C.name
FROM customers               C
         LEFT JOIN locations l ON C.id = l.id_customers
WHERE l.id IS NULL
ORDER BY C.ID;