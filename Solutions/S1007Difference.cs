using System;

namespace Solutions {
    public class S1007Difference {

        public static void Main(string[] args) {
            var a = InputReader.ReadInt();
            var b = InputReader.ReadInt();
            var c = InputReader.ReadInt();
            var d = InputReader.ReadInt();
            
            Console.WriteLine(CalculateResult(a, b, c, d));
        }
        public static string CalculateResult(int a, int b, int c, int d) {
            var ab = a * b;
            var cd = c * d;
            return $"DIFERENCA = {ab - cd}";
        }


        private class InputReader {

            // This class would, ideally, be re-used between multiple solutions. However, due to the nature of the 
            // beecrowd platform, we cannot have more than one file uploaded per submission.
            public static double ReadDouble() {
                var line = Console.ReadLine();

                if (line == null || !double.TryParse(line, out var result)) {
                    throw new NullReferenceException($"Line cannot be parsed as double: {line}");
                }

                return result;
            }

            public static int ReadInt() {
                var line = Console.ReadLine();

                if (line == null || !int.TryParse(line, out var result)) {
                    throw new NullReferenceException($"Line cannot be parsed as int: {line}");
                }

                return result;
            }

        }

    }
}
