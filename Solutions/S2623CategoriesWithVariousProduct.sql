SELECT P.name, C.name
FROM products                  P
         INNER JOIN categories C ON C.id = P.id_categories
WHERE P.amount > 100 AND C.id IN (1, 2, 3, 6, 9)
ORDER BY C.id;