WITH Stats AS (
              SELECT MAX(customers_number)              AS maximum,
                     MIN(customers_number)              AS minimum,
                     CAST(AVG(customers_number) AS INT) AS average
              FROM lawyers
              ),
Results AS (
                SELECT 0 AS sort, name, customers_number
                FROM lawyers
                         INNER JOIN Stats ON lawyers.customers_number = maximum
                UNION
                SELECT 1 AS sort, name, customers_number
                FROM lawyers
                         INNER JOIN Stats ON lawyers.customers_number = minimum
                UNION
                SELECT 2 AS sort, 'Average' AS name, average AS customers_number
                FROM Stats
                )
SELECT name, customers_number
FROM Results
ORDER BY sort;