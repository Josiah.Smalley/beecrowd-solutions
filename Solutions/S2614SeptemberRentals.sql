SELECT name, rentals_date
FROM customers
         INNER JOIN rentals r ON customers.id = r.id_customers
WHERE rentals_date >= '2016-09-01'
  AND rentals_date < '2016-10-01';