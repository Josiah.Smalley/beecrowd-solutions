WITH Precalculation AS (
                       SELECT name,
                              COUNT(*)                                                       AS matches,
                              SUM(CASE
                                      WHEN ((team_1 = teams.id AND team_1_goals > team_2_goals) OR
                                            (team_2 = teams.id AND team_1_goals < team_2_goals)) THEN 1
                                      ELSE 0 END)                                            AS victories,
                              SUM(CASE WHEN (team_1_goals = team_2_goals) THEN 1 ELSE 0 END) AS draws
                       FROM teams
                                INNER JOIN matches ON teams.id = matches.team_1 OR teams.id = matches.team_2
                       GROUP BY name
                       )
SELECT name, matches, victories, matches - victories - draws AS defeats, draws, 3 * victories + draws AS score
FROM Precalculation
ORDER BY score DESC;