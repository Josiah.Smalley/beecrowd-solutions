using System;

namespace Solutions {
    public class S1039FireFlowers {

        public static void Main(string[] args) {
            while (!InputReader.IsEof) {
                Console.WriteLine(CalculateSingleResult(ReadProblemLine()));
            }
        }

        private static ProblemLine ReadProblemLine() =>
            new ProblemLine() {
                HunterRadius = InputReader.NextInt(),
                HunterX = InputReader.NextInt(),
                HunterY = InputReader.NextInt(),
                FlowerRadius = InputReader.NextInt(),
                FlowerX = InputReader.NextInt(),
                FlowerY = InputReader.NextInt(),
            };

        public static string CalculateSingleResult(ProblemLine problemLine) {
            var distanceBetweenEntities = Math.Sqrt(Math.Pow(problemLine.HunterX - problemLine.FlowerX, 2) +
                Math.Pow(problemLine.HunterY - problemLine.FlowerY, 2)
            );

            var isSuccess = (distanceBetweenEntities + problemLine.FlowerRadius) <= problemLine.HunterRadius;

            return isSuccess ? "RICO" : "MORTO";
        }

        public struct ProblemLine {

            public int HunterRadius;
            public int HunterX;
            public int HunterY;
            public int FlowerRadius;
            public int FlowerX;
            public int FlowerY;

        }

        private class InputReader {

            // This class would, ideally, be re-used between multiple solutions. However, due to the nature of the 
            // beecrowd platform, we cannot have more than one file uploaded per submission.

            private static string[] _sections = Array.Empty<string>();
            private static int _index;

            public static bool IsEof {
                get {
                    if (HasNextSection) {
                        return false;
                    }

                    LoadSections();
                    return !HasNextSection;
                }

            }


            public static bool HasNextSection => _index < _sections.Length;

            public static void LoadSections() {
                try {
                    _sections = ReadLine().Split(' ');
                    _index = 0;
                } catch (NullReferenceException) {
                    _sections = Array.Empty<string>();
                    _index = 0;
                }
            }

            public static void Skip(int count = 1) {
                while (count > 0) {
                    if (_index + count <= _sections.Length) {
                        _index += count;
                        count = 0;
                        continue;
                    }

                    count -= _sections.Length - _index;
                    LoadSections();
                }
            }

            public static string NextSection() {
                if (!HasNextSection) {
                    LoadSections();
                }

                if (!HasNextSection) {
                    throw new NullReferenceException("Unable to load the next section");
                }

                var result = _sections[_index];
                _index++;
                return result;
            }

            public static double ReadDouble() {
                var line = Console.ReadLine();

                if (line == null || !double.TryParse(line, out var result)) {
                    throw new NullReferenceException($"Line cannot be parsed as double: {line}");
                }

                return result;
            }

            public static int ReadInt() {
                var line = Console.ReadLine();

                if (line == null || !int.TryParse(line, out var result)) {
                    throw new NullReferenceException($"Line cannot be parsed as int: {line}");
                }

                return result;
            }

            public static string ReadLine() {
                var line = Console.ReadLine();

                if (line == null) {
                    throw new NullReferenceException($"Line is null");
                }

                return line;
            }

            public static int NextInt() {
                var section = NextSection();

                if (!int.TryParse(section, out var result)) {
                    throw new NullReferenceException($"Line cannot be parsed as int: {section}");
                }

                return result;
            }

            public static double NextDouble() {

                var section = NextSection();

                if (!double.TryParse(section, out var result)) {
                    throw new NullReferenceException($"Line cannot be parsed as double: {section}");
                }

                return result;
            }

        }


    }

}
