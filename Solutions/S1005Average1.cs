using System;

namespace Solutions {
    public class S1005Average1 {

        private const double WeightA = 3.5;
        private const double WeightB = 7.5;
        private const double WeightTotal = WeightA + WeightB;

        public static void Main(string[] args) {
            var a = InputReader.ReadDouble();
            var b = InputReader.ReadDouble();

            Console.WriteLine(CalculateResult(a, b));
        }

        public static string CalculateResult(double a, double b) {
            var weightedA = a * WeightA;
            var weightedB = b * WeightB;
            var total = (weightedA + weightedB) / WeightTotal;

            return $"MEDIA = {total:f5}";
        }

        private class InputReader {

            // This class would, ideally, be re-used between multiple solutions. However, due to the nature of the 
            // beecrowd platform, we cannot have more than one file uploaded per submission.
            public static double ReadDouble() {
                var line = Console.ReadLine();

                if (line == null || !double.TryParse(line, out var result)) {
                    throw new NullReferenceException($"Line cannot be parsed as double: {line}");
                }

                return result;
            }

        }

    }
}
