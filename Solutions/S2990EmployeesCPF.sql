SELECT cpf, enome, dnome
FROM empregados
         INNER JOIN departamentos d ON empregados.dnumero = d.dnumero
         LEFT JOIN  trabalha      t ON t.cpf_emp = cpf
WHERE cpf_emp IS NULL
ORDER BY cpf;