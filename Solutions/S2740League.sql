-- There should probably be contingencies in case there are less than 5 teams, but this works for now
WITH DemotedCutoff AS (
                      SELECT MAX(position) - 1 AS firstDemoted
                      FROM league
                      )
SELECT CONCAT(CASE WHEN position < 4 THEN 'Podium' ELSE 'Demoted' END, ': ', team) AS name
FROM league
         CROSS JOIN DemotedCutoff
WHERE position < 4
   OR position >= firstDemoted;
