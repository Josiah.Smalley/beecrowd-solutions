using NUnit.Framework;
using Solutions;

namespace SolutionTests {
    public class T1005Average1 {

        [Test]
        public void Sample1() {
            const double a = 5.0;
            const double b = 7.1;
            const string expected = "MEDIA = 6.43182";
            var actual = S1005Average1.CalculateResult(a, b);
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Sample2() {
            const double a = 0.0;
            const double b = 7.1;
            const string expected = "MEDIA = 4.84091";
            var actual = S1005Average1.CalculateResult(a, b);
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Sample3() {
            const double a = 10.0;
            const double b = 10.0;
            const string expected = "MEDIA = 10.00000";
            var actual = S1005Average1.CalculateResult(a, b);
            Assert.That(actual, Is.EqualTo(expected));
        }

    }
}
