using NUnit.Framework;

namespace SolutionTests {
    public class T1009SalaryWithBonus {

        [Test]
        public void Sample1() {
            const string name = "JOAO";
            const double salary = 500.00;
            const double sales = 1230.30;
            const string expected = "TOTAL = R$ 684.54";
            var actual = Solutions.S1009SalaryWithBonus.CalculateResult(name, salary, sales);
            Assert.That(actual, Is.EqualTo(expected));
        }
        
        [Test]
        public void Sample2() {
            const string name = "PEDRO";
            const double salary = 700.00;
            const double sales = 0.00;
            const string expected = "TOTAL = R$ 700.00";
            var actual = Solutions.S1009SalaryWithBonus.CalculateResult(name, salary, sales);
            Assert.That(actual, Is.EqualTo(expected));
        }
        
        [Test]
        public void Sample3() {
            const string name = "MANGOJATA";
            const double salary = 1700.00;
            const double sales = 1230.50;
            const string expected = "TOTAL = R$ 1884.58";
            var actual = Solutions.S1009SalaryWithBonus.CalculateResult(name, salary, sales);
            Assert.That(actual, Is.EqualTo(expected));
        }

    }
}
