using NUnit.Framework;

namespace SolutionTests {
    public class T1008Salary {

        [Test]
        public void Sample1() {
            const int employeeNumber = 25;
            const int hoursWorked = 100;
            const double hourlyWage = 5.50;
            const string expected = "NUMBER = 25\nSALARY = U$ 550.00";
            var actual = Solutions.S1008Salary.CalculateResult(employeeNumber, hoursWorked, hourlyWage);
            Assert.That(actual, Is.EqualTo(expected));
        }
        
        [Test]
        public void Sample2() {
            const int employeeNumber = 1;
            const int hoursWorked = 200;
            const double hourlyWage = 20.50;
            const string expected = "NUMBER = 1\nSALARY = U$ 4100.00";
            var actual = Solutions.S1008Salary.CalculateResult(employeeNumber, hoursWorked, hourlyWage);
            Assert.That(actual, Is.EqualTo(expected));
        }
        
        [Test]
        public void Sample3() {
            const int employeeNumber = 6;
            const int hoursWorked = 145;
            const double hourlyWage = 15.55;
            const string expected = "NUMBER = 6\nSALARY = U$ 2254.75";
            var actual = Solutions.S1008Salary.CalculateResult(employeeNumber, hoursWorked, hourlyWage);
            Assert.That(actual, Is.EqualTo(expected));
        }

    }
}
