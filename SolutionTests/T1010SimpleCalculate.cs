using NUnit.Framework;

namespace SolutionTests {
    public class T1010SimpleCalculate {

        [Test]
        public void Sample1() {
            const int units1 = 1;
            const double price1 = 5.30;
            const int units2 = 2;
            const double price2 = 5.10;
            const string expected = "VALOR A PAGAR: R$ 15.50";
            var actual = Solutions.S1010SimpleCalculate.CalculateResult(units1, price1, units2, price2);
            Assert.That(actual, Is.EqualTo(expected));
        }
        
        [Test]
        public void Sample2() {
            const int units1 = 2;
            const double price1 = 15.30;
            const int units2 = 4;
            const double price2 = 5.20;
            const string expected = "VALOR A PAGAR: R$ 51.40";
            var actual = Solutions.S1010SimpleCalculate.CalculateResult(units1, price1, units2, price2);
            Assert.That(actual, Is.EqualTo(expected));
        }
        
        [Test]
        public void Sample3() {
            const int units1 = 1;
            const double price1 = 15.10;
            const int units2 = 1;
            const double price2 = 15.10;
            const string expected = "VALOR A PAGAR: R$ 30.20";
            var actual = Solutions.S1010SimpleCalculate.CalculateResult(units1, price1, units2, price2);
            Assert.That(actual, Is.EqualTo(expected));
        }

    }
}
