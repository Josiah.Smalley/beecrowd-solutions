using NUnit.Framework;
using Solutions;

namespace SolutionTests {
    public class T1003SimpleSum {

        [Test]
        public void Sample1() {
            const int a = 30;
            const int b = 10;
            const string expected = "SOMA = 40";
            var actual = S1003SimpleSum.CalculateResult(a, b);
            Assert.That(actual, Is.EqualTo(expected));
        }
        [Test]
        public void Sample2() {
            const int a = -30;
            const int b = 10;
            const string expected = "SOMA = -20";
            var actual = S1003SimpleSum.CalculateResult(a, b);
            Assert.That(actual, Is.EqualTo(expected));
        }
        [Test]
        public void Sample3() {
            const int a = 0;
            const int b = 0;
            const string expected = "SOMA = 0";
            var actual = S1003SimpleSum.CalculateResult(a, b);
            Assert.That(actual, Is.EqualTo(expected));
        }

    }
}
