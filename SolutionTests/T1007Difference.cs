using NUnit.Framework;
using Solutions;

namespace SolutionTests {
    public class T1007Difference {

        [Test]
        public void Sample1() {
            const int a = 5;
            const int b = 6;
            const int c = 7;
            const int d = 8;
            const string expected = "DIFERENCA = -26";
            var actual = S1007Difference.CalculateResult(a, b, c, d);
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Sample2() {
            const int a = 0;
            const int b = 0;
            const int c = 7;
            const int d = 8;
            const string expected = "DIFERENCA = -56";
            var actual = S1007Difference.CalculateResult(a, b, c, d);
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Sample3() {
            const int a = 5;
            const int b = 6;
            const int c = -7;
            const int d = 8;
            const string expected = "DIFERENCA = 86";
            var actual = S1007Difference.CalculateResult(a, b, c, d);
            Assert.That(actual, Is.EqualTo(expected));
        }

    }
}
