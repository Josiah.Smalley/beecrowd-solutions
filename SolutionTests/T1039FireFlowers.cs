using System.Xml;
using NUnit.Framework;
using Solutions;

namespace SolutionTests {
    public class T1039FireFlowers {

        [Test]
        public void Sample1() {
            var problemLine = new S1039FireFlowers.ProblemLine {
                HunterRadius = 6,
                HunterX = -8,
                HunterY = 2,
                FlowerRadius = 3,
                FlowerX = 0,
                FlowerY = 0
            };

            const string expected = "MORTO";
            var actual = S1039FireFlowers.CalculateSingleResult(problemLine);
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Sample2() {
            var problemLine = new S1039FireFlowers.ProblemLine {
                HunterRadius = 7,
                HunterX = 3,
                HunterY = 4,
                FlowerRadius = 2,
                FlowerX = 4,
                FlowerY = 5
            };

            const string expected = "RICO";
            var actual = S1039FireFlowers.CalculateSingleResult(problemLine);
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Sample3() {
            var problemLine = new S1039FireFlowers.ProblemLine {
                HunterRadius = 3,
                HunterX = 0,
                HunterY = 0,
                FlowerRadius = 4,
                FlowerX = 0,
                FlowerY = 0
            };

            const string expected = "MORTO";
            var actual = S1039FireFlowers.CalculateSingleResult(problemLine);
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Sample4() {
            var problemLine = new S1039FireFlowers.ProblemLine {
                HunterRadius = 5,
                HunterX = 4,
                HunterY = 7,
                FlowerRadius = 1,
                FlowerX = 8,
                FlowerY = 7
            };

            const string expected = "RICO";
            var actual = S1039FireFlowers.CalculateSingleResult(problemLine);
            Assert.That(actual, Is.EqualTo(expected));
        }

    }
}
