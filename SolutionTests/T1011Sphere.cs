using NUnit.Framework;

namespace SolutionTests {
    public class T1011Sphere {

        [Test]
        public void Sample1() {
            const int radius = 3;
            const string expected = "VOLUME = 113.097";
            var actual = Solutions.S1011Sphere.CalculateResult(radius);
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Sample2() {
            const int radius = 15;
            const string expected = "VOLUME = 14137.155";
            var actual = Solutions.S1011Sphere.CalculateResult(radius);
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Sample3() {
            const int radius = 1523;
            const string expected = "VOLUME = 14797486501.627";
            var actual = Solutions.S1011Sphere.CalculateResult(radius);
            Assert.That(actual, Is.EqualTo(expected));
        }

    }
}
