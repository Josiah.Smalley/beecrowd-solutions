using NUnit.Framework;
using Solutions;

namespace SolutionTests {
    public class T1003SimpleProduct {

        [Test]
        public void Sample1() {
            const int a = 3;
            const int b = 9;
            const string expected = "PROD = 27";
            var actual = S1004SimpleProduct.CalculateResult(a, b);
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Sample2() {
            const int a = -30;
            const int b = 10;
            const string expected = "PROD = -300";
            var actual = S1004SimpleProduct.CalculateResult(a, b);
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Sample3() {
            const int a = 0;
            const int b = 9;
            const string expected = "PROD = 0";
            var actual = S1004SimpleProduct.CalculateResult(a, b);
            Assert.That(actual, Is.EqualTo(expected));
        }

    }
}
