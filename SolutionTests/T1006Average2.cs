using NUnit.Framework;
using Solutions;

namespace SolutionTests {
    public class T1006Average2 {

        [Test]
        public void Sample1() {
            const double a = 5.0;
            const double b = 6.0;
            const double c = 7.0;
            const string expected = "MEDIA = 6.3";
            var actual = S1006Average2.CalculateResult(a, b, c );
            Assert.That(actual, Is.EqualTo(expected));
        }
        [Test]
        public void Sample2() {
            const double a = 5.0;
            const double b = 10.1;
            const double c = 10.0;
            const string expected = "MEDIA = 9.0";
            var actual = S1006Average2.CalculateResult(a, b, c );
            Assert.That(actual, Is.EqualTo(expected));
        }
        [Test]
        public void Sample3() {
            const double a = 10.0;
            const double b = 10.0;
            const double c = 5.0;
            const string expected = "MEDIA = 7.5";
            var actual = S1006Average2.CalculateResult(a, b, c );
            Assert.That(actual, Is.EqualTo(expected));
        }

    }
}
