using NUnit.Framework;
using Solutions;

namespace SolutionTests {
    public class T1002AreaOfACircle {

        [Test]
        public void Sample1() {
            const double input = 2.00;
            const string expected = "A=12.5664";
            var actual = S1002AreaOfACircle.CalculateResult(input);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Sample2() {
            const double input = 100.64;
            const string expected = "A=31819.3103";
            var actual = S1002AreaOfACircle.CalculateResult(input);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Sample3() {
            const double input = 150.00;
            const string expected = "A=70685.7750";
            var actual = S1002AreaOfACircle.CalculateResult(input);

            Assert.That(actual, Is.EqualTo(expected));
        }
    }
}
